import time
import datetime
from datetime import date
import subprocess
import requests
import pandas as pd
import json
import schedule
import numpy
from requests.auth import HTTPDigestAuth
from requests import Session
from slackclient import SlackClient

SLACK_TOKEN = 'xoxb-947439552053-947459916261-54GsmiYHUFdDbbpPrsEtnTn6'
slack_client = SlackClient(SLACK_TOKEN)
channel_id = 'CTHTZ6JE7'


def file_upload(title, option):
    slack_client.api_call(
        "files.upload",
        filename=title,
        username='Tech Bot',
        channels=channel_id,
        file=open(option, 'r')
    )


def list_channels():
    channels_call = slack_client.api_call("channels.list")
    if channels_call['ok']:
        return channels_call['channels']
    return None


def list_messages():
    messages_call = slack_client.api_call("im.list")
    if messages_call['ok']:
        return messages_call['ims']
    return None


def channel_info(channel_id):
    channel_info = slack_client.api_call("conversations.info", channel=channel_id)
    if channel_info:
        return channel_info['channel']
    return None


def send_message(channel_id, message):
    slack_client.api_call(
        "chat.postMessage",
        link_names=1,
        channel=channel_id,
        text=message,
        username='pythonbot',
        icon_emoji=':robot_face:'
    )


def list_rma():
    # ont = pd.read_csv('')
    try:
        mbtext = open('missingboards.txt', 'r')
        latext = open('lowasics.txt', 'r')
        mb = '*Machine(s) w/ missing boards and within RMA:*\n*IP Address*         *Expiration Date*\n'
        la = "*Machine(s) w/ low ASIC's and within RMA:*\n*IP Address*         *Expiration Date*\n"

        mbcount = 0
        for machine in mbtext:
            if '10.10.35.49' in machine.strip('\n')  :
                pass
            else:
                mbcount += 1
                mb = mb + machine
        if mbcount < 1:
            mb = ''
        elif mbcount > 0:
            mb = '*'+str(mbcount)+'* ' + mb

        lacount = 0
        for machine in latext:
            lacount += 1
            la = la + machine
        if lacount < 1:
            la = ''
        elif lacount > 0:
            la = '*'+str(lacount)+'* ' + la

        latext.close()
        mbtext.close()
        if mbcount < 1 and lacount < 1:
            message = "*Currently no issues with machines eligible for RMA!*"
        else:
            message = mb + '\n\n' + la
        return message

    except:
        return str(e)




def display_time(seconds, granularity=4):
    result = []
    intervals = (
        ('weeks', 604800),  # 60 * 60 * 24 * 7
        ('days', 86400),  # 60 * 60 * 24
        ('hours', 3600),  # 60 * 60
        ('minutes', 60),
        ('seconds', 1),
    )

    for name, count in intervals:
        value = seconds // count
        if value:
            seconds -= value * count
            if value == 1:
                name = name.rstrip('s')
            result.append("{} {}".format(value, name))
    return ', '.join(result[:granularity])


def expiryCheck():
    ont1 = pd.read_csv('G:/My Drive/tracker.csv', dtype=object)
    ont = ont1.replace(numpy.nan, '', regex=True)
    z = datetime.datetime.now()
    yz = z.strftime('%m/%d/%Y')
    todayYear = int(yz.split('/')[2])
    todayDay = int(yz.split('/')[1])
    todayMonth = int(yz.split('/')[0])
    expireYear = 0
    expireDay = 0
    expireMonth = 0
    row = 0
    count = 0
    expireSoon = []
    file = open('expiring.txt', 'w')
    for i in ont['expiryDate']:
        if i == '':
            pass
        else:
            expireYear = int(i.split('/')[2])
            expireDay = int(i.split('/')[1])
            expireMonth = int(i.split('/')[0])
            try:
                remaining = date(expireYear, expireMonth, expireDay) - date(todayYear, todayMonth, todayDay)
                if remaining.days in range(0, 30):
                    count += 1
                    expireSoon.append(ont.at[row, 'IP Address'])
                    file.write(ont.at[row, 'IP Address'] + ' | ' + str(remaining.days) + ' days left\n')
            except Exception as e:
                print('Failed: ' + str(e))
        row += 1
    file.close()

    return count


if __name__ == '__main__':

    channelsList = []
    channels = list_channels()
    messages = list_messages()

    for imchannel in messages:
        channel_id = imchannel['id']
        channelsList.append(channel_id)
    print('Slack Bot online!')
    print('Listening to ' + str(len(channelsList)) + ' channels...')

    while True:
        for channel_id in channelsList:
            try:
                x = datetime.datetime.now()
                detailed_info = channel_info(channel_id)
                if detailed_info['latest']['text']:
                    if detailed_info['user'] != 'USLACKBOT':
                        if '.cmd' in detailed_info['latest']['text']:
                            send_message(channel_id, '*Commands for Tech Bot*\n'
                                            '---------------------------------------------------------------------------------\n'
                                            '*.info*        | _display info on an Antminer by specifying its IP_\n'
                                            '*.server*    | _online status of the Linux Server in the core room_\n'
                                            "*.rma*        | _sends user a list of machines with issues that could be RMA'd_\n"
                                            "*.audit*      | _sends user CSV outputs of the Tracker Auditor program_\n")
                            send_message(channel_id, '---------------------------------------------------------------------------------')
                            pass
                        if '.info' in detailed_info['latest']['text']:
                            if len(detailed_info['latest']['text'].split(' ')) == 2:
                                ip = detailed_info['latest']['text'].split(' ')[1]
                                if '10.10.' not in ip:
                                    ip = '10.10.' + ip
                                url = 'http://' + ip + '/cgi-bin/get_miner_status.cgi'
                                url2 = 'http://' + ip + '/cgi-bin/get_system_info.cgi'
                                s = requests.Session()
                                hashrate = ''
                                machinetype = ''
                                uptime = ''

                                try:
                                    r = s.post(url, auth=HTTPDigestAuth('root', 'root'), timeout=3)
                                    data = json.loads(r.text)
                                    hashrate = str(data['summary']['ghs5s'])
                                    uptime = display_time(int(data['summary']['elapsed']))
                                    try:
                                        p1 = subprocess.Popen(['ping ', '-n', '1', ip], stdout=subprocess.PIPE)
                                        p1.wait()
                                        if p1.poll():
                                            send_message(channel_id, ip + ' is *offline*')
                                            pass
                                        else:
                                            r = s.post(url2, auth=HTTPDigestAuth('root', 'root'), timeout=3)
                                            data = json.loads(r.text)
                                            machinetype = str(data['minertype'])
                                            send_message(channel_id, '*' + ip + ' | ' + machinetype + ' | Uptime: ' +
                                                         uptime + ' | Hashrate: ' + hashrate + ' GH/s*')
                                            pass
                                    except Exception as e:
                                        print(str(e))
                                        pass

                                except Exception as e:
                                    try:
                                        p1 = subprocess.Popen(['ping ', '-n', '1', ip], stdout=subprocess.PIPE)
                                        p1.wait()
                                        if p1.poll():
                                            send_message(channel_id, ip + ' is *offline*')
                                            pass
                                        else:
                                            send_message(channel_id, ip + ' is *online*, but is not an Antminer')
                                            pass

                                    except Exception as e:
                                        print(str(e))
                                        pass
                            else:
                                # send_message(channel_id, '*Invalid command*')
                                pass

                        elif '.server' == detailed_info['latest']['text']:
                            ping = '10.20.0.20'
                            p1 = subprocess.Popen(['ping ', "-n", "1", ping], stdout=subprocess.PIPE)
                            p1.wait()
                            if p1.poll():
                                # OFFLINE
                                ping = '10.20.0.10'
                                p1 = subprocess.Popen(['ping', '-n', '1', ping], stdout=subprocess.PIPE)
                                p1.wait()
                                if p1.poll():
                                    send_message(channel_id, 'Physical VM server is *offline!!*')
                                    pass
                                else:
                                    send_message(channel_id, 'Linux server is *offline*, but the physical server'
                                                                ' is *online*')
                                    pass
                            else:
                                # ONLINE
                                send_message(channel_id, 'Linux server is *online!*')
                                pass

                        elif '.rma' == detailed_info['latest']['text']:
                            user = detailed_info['latest']['user']
                            send_message(user, list_rma())
                            pass

                        elif '.audit' in detailed_info['latest']['text']:
                            user = detailed_info['latest']['user']
                            title = 'onlineTrackerAudit.csv'
                            option = 'onlineTrackerAudit.csv'
                            send_message(user, file_upload(title, option))

                            title = 'offlineTrackerAudit.csv'
                            option = 'offlineTrackerAudit.csv'
                            send_message(user, file_upload(title, option))
                            send_message(channel_id, 'Tracker Audits sent!')
                            pass

            except Exception as e:
                pass

        time.sleep(.5)
    else:
        print("Unable to authenticate.")
