from slackeventsapi import SlackEventAdapter
from slackclient import SlackClient
import os
import time
import datetime
import http
from datetime import date
import subprocess
import requests
import pandas as pd
import json
import numpy
import threading
from requests.auth import HTTPDigestAuth

# Our app's Slack Event Adapter for receiving actions via the Events API #
SLACK_SIGNING_SECRET = 'c0a3e1f2186d57e5e33d1b0488244b5d'  # Under 'Basic Info' on Slack App page
SLACK_BOT_TOKEN = 'xoxb-947439552053-947459916261-54GsmiYHUFdDbbpPrsEtnTn6'  # OAuth Token / 'OAuth & Permissions'

# Keys for Projspok Slack, should only work on the Tracker Updater laptop? #
# SLACK_SIGNING_SECRET = '8a4f713d983fdab7c73e04ef2c21e78c'
# SLACK_BOT_TOKEN = 'xoxb-335283337843-939312442945-gjVUcCQEKAxdmdvezGzEzkLk'

# Create a SlackClient for your bot to use for Web API requests #
slack_events_adapter = SlackEventAdapter(SLACK_SIGNING_SECRET, "/slack/events")
slack_client = SlackClient(SLACK_BOT_TOKEN)


# Command Functions #
class Commands:
    temp = None

    def list_rma(self):
        # ont = pd.read_csv('')
        try:
            mbtext = open('missingboards.txt', 'r')
            latext = open('lowasics.txt', 'r')
            zhtext = open('zerohash.txt', 'r')
            mb = '*Machine(s) w/ missing boards and within RMA:*\n*IP Address*         *Expiration Date*\n'
            la = "*Machine(s) w/ low ASIC's and within RMA:*\n*IP Address*         *Expiration Date*\n"
            zh = "*Machine(s) w/ 0 hashrate, an uptime of 30 or more minutes, and within RMA:*\n*IP Address*         *Expiration Date*\n"

            mbcount = 0
            for machine in mbtext:
                if '10.10.35.49' in machine.strip('\n'):
                    pass
                else:
                    mbcount += 1
                    mb = mb + machine
            if mbcount < 1:
                mb = ''
            elif mbcount > 0:
                mb = '*'+str(mbcount)+'* ' + mb

            lacount = 0
            for machine in latext:
                lacount += 1
                la = la + machine
            if lacount < 1:
                la = ''
            elif lacount > 0:
                la = '*'+str(lacount)+'* ' + la

            zhcount = 0
            for machine in zhtext:
                zhcount += 1
                zh = zh + machine
            if zhcount < 1:
                zh = ''
            elif zhcount > 0:
                zh = '*'+str(zhcount)+'* ' + zh

            zhtext.close()
            latext.close()
            mbtext.close()
            if mbcount < 1 and lacount < 1 and zhcount < 1:
                message = "*Currently no issues with machines eligible for RMA!*"
            else:
                message = mb + '\n\n' + la + '\n\n' + zh
            return message

        except Exception as e:
            return str(e)

    def file_upload(self, title, option):
        slack_client.api_call(
            "files.upload",
            filename=title,
            username='Tech Bot',
            channels=channel_id,
            file=open(option, 'r')
        )

    def serverStatus(self, channel):
        ping = '10.20.0.20'
        p1 = subprocess.Popen(['ping ', "-n", "1", ping], stdout=subprocess.PIPE)
        p1.wait()
        if p1.poll():
            # OFFLINE
            ping = '10.20.0.10'
            p1 = subprocess.Popen(['ping', '-n', '1', ping], stdout=subprocess.PIPE)
            p1.wait()
            if p1.poll():
                slack_client.api_call('chat.postMessage', channel=channel, text='Physical VM server is *offline!!*')
                return

            else:
                slack_client.api_call('chat.postMessage', channel=channel, text='Linux server is *offline*, '
                                                                                'but the physical server is *online*')
                return
        else:
            # ONLINE
            slack_client.api_call('chat.postMessage', channel=channel, text='Linux server is *online!*')
            return

    def machineInfo(self, ip, channel):
        if '10.10.' not in ip:
            ip = '10.10.' + ip
        url = 'http://' + ip + '/cgi-bin/get_miner_status.cgi'
        url2 = 'http://' + ip + '/cgi-bin/get_system_info.cgi'
        s = requests.Session()
        hashrate = ''
        machinetype = ''
        uptime = ''

        try:
            ping = ip
            p1 = subprocess.Popen(['ping', '-n', '1', ping], stdout=subprocess.PIPE)
            p1.wait()
            if p1.poll():
                slack_client.api_call('chat.postMessage', channel=channel, text=ip + ' is *offline*')
            elif not p1.poll():
                try:
                    r1 = s.post(url, auth=HTTPDigestAuth('root', 'root'), timeout=3)
                    r2 = s.post(url2, auth=HTTPDigestAuth('root', 'root'), timeout=3)
                    data1 = json.loads(r1.text)
                    data2 = json.loads(r2.text)
                    hashrate = str(data1['summary']['ghs5s'])
                    uptime = Commands().display_time(int(data1['summary']['elapsed']))
                    machinetype = str(data2['minertype'])
                    slack_client.api_call('chat.postMessage', channel=channel, text='*' + ip + ' | ' + machinetype +
                                                                            ' | Uptime: ' + uptime + ' | Hashrate: ' +
                                                                            hashrate + ' GH/s*')
                except Exception as e:
                    print('failed getting miner info for ' + ip + ' ' + str(e))
        except Exception as e:
            print('unable to ping ' + ip + ' ' + str(e))


    def display_time(self, seconds, granularity=4):
        result = []
        intervals = (
            ('weeks', 604800),  # 60 * 60 * 24 * 7
            ('days', 86400),  # 60 * 60 * 24
            ('hours', 3600),  # 60 * 60
            ('minutes', 60),
            ('seconds', 1),
        )

        for name, count in intervals:
            value = seconds // count
            if value:
                seconds -= value * count
                if value == 1:
                    name = name.rstrip('s')
                result.append("{} {}".format(value, name))
        return ', '.join(result[:granularity])


# Actions for when messages are sent to bot #
@slack_events_adapter.on("message")
def handle_message(event_data):
    message = event_data["event"]
    if Commands.temp is None:
        Commands.temp = message.get('text')
    elif Commands.temp == message.get('text'):
        return
    else:
        Commands.temp = message.get('text')

    response = http.HTTPStatus(200)
    channel = message['channel']

    if message.get("subtype") is None and not message.get('bot_id') and message.get('text').lower() == '.rma':
        slack_client.api_call("chat.postMessage", channel=channel, text=Commands().list_rma())
        return response

    elif message.get("subtype") is None and not message.get('bot_id') and message.get('text').lower() == '.cmd' or message.get('text').lower() == '.commands':
        slack_client.api_call("chat.postMessage", channel=channel, text='*Commands for Tech Bot*\n'
                                '---------------------------------------------------------------------------------\n'
                                '*.info*        | _display info on an Antminer by specifying its IP_\n'
                                '*.server*    | _online status of the Linux Server in the core room_\n'
                                "*.rma*        | _sends user a list of machines with issues that could be RMA'd_\n"
                                "*.audit*      | _sends user CSV outputs of the Tracker Auditor program_\n"
                                "---------------------------------------------------------------------------------")
        return response

    elif message.get("subtype") is None and not message.get('bot_id') and ".info" in message.get('text').lower():
        command = message.get('text').split(' ')
        if len(command) != 2:
            slack_client.api_call("chat.postMessage", channel=channel, text='Invalid parameters')
            return response

        elif len(command) == 2:
            ip = command[1]
            info = threading.Thread(target=Commands().machineInfo, args=(ip, channel,))
            info.start()
            return response

        return response

    elif message.get("subtype") is None and not message.get('bot_id') and message.get('text').lower() == '.audit':
        title = 'onlineTrackerAudit.csv'
        option = 'onlineTrackerAudit.csv'
        title2 = 'offlineTrackerAudit.csv'
        option2 = 'offlineTrackerAudit.csv'

        slack_client.api_call('files.upload', filename=title, channels=channel, file=open(option, 'r'))
        slack_client.api_call('files.upload', filename=title2, channels=channel, file=open(option2, 'r'))
        return response

    elif message.get("subtype") is None and not message.get('bot_id') and message.get('text').lower() == '.server':
        ping = threading.Thread(target=Commands().serverStatus, args=(channel,))
        ping.start()
        return response
    return response


# Actions for when emojis are sent to bot? #
@slack_events_adapter.on("reaction_added")
def reaction_added(event_data):
    event = event_data["event"]
    emoji = event["reaction"]
    channel = event["item"]["channel"]
    text = ":%s:" % emoji
    slack_client.api_call("chat.postMessage", channel=channel, text=text)


# Error events
@slack_events_adapter.on("error")
def error_handler(err):
    print("ERROR: " + str(err))


# Once we have our event listeners configured, we can start the
# Flask server with the default `/events` endpoint on port 3000
slack_events_adapter.start(port=3000)
