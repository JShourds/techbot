import pymysql
import time
import json
import os
import numpy
import pandas as pd
import datetime
import schedule
import requests
from requests.auth import HTTPDigestAuth
from datetime import date


def validRMA():

    ont1 = pd.read_csv('G:/My Drive/tracker.csv', dtype=object)
    ont = ont1.replace(numpy.nan, '', regex=True)
    row = 0
    count = 0
    stillValid = []
    allmissing = []
    z = datetime.datetime.now()
    yz = z.strftime('%m/%d/%Y')
    date_time = datetime.datetime.now().strftime('%Y-%m-%d %H')
    date_time = date_time + ':00:00'
    print(date_time)
    todayYear = int(yz.split('/')[2])
    todayDay = int(yz.split('/')[1])
    todayMonth = int(yz.split('/')[0])
    for i in ont['expiryDate']:
        if i == '':
            pass
        else:
            expireYear = int(i.split('/')[2])
            expireDay = int(i.split('/')[1])
            expireMonth = int(i.split('/')[0])
            try:
                remaining = date(expireYear, expireMonth, expireDay) - date(todayYear, todayMonth, todayDay)
                if remaining.days > 1:
                    stillValid.append(ont.at[row, 'IP Address'])
            except Exception as e:
                print('Failed: ' + str(e))
        row += 1
    file = open('missingboards.txt', 'w')
    try:
        conn = pymysql.connect(host='10.20.0.220', port=3306, user='Justin', password='Password', db='Tracker')
        cursor = conn.cursor()


    except Exception as e:
        print("Database not connectable... No data saved. " + str(e))
        return

    for ip in stillValid:
        exstr = """SELECT missingBoards FROM `Issues_History` WHERE date_time = %s"""
        cursor.execute(exstr, date_time)
    rmamachines = cursor.fetchall()
    for tuple in rmamachines:
        for stuff in tuple:
            stuff = stuff.split('[')[1].split(']')[0].replace("'", '').split(', ')
            allmissing = stuff
            for i in allmissing:
                for x in stillValid:
                    if x == i:
                        ipcount = -1
                        for ip in ont['IP Address']:
                            if x == ip:
                                ipcount += 1
                                x = "<http://root:root@" + x + "/cgi-bin/minerStatus.cgi|" + x + ">"
                                file.write(x + '   *|*   ' + ont.at[ipcount, 'expiryDate'] + '\n')
                            else:
                                ipcount += 1
                        count += 1
    print(str(count) + ' machines with missing boards and valid RMA dates')
    file.close()

    count = 0
    file = open('lowasics.txt', 'w')
    for ip in stillValid:
        exstr = """SELECT lowASICs FROM `Issues_History` WHERE date_time = %s"""
        cursor.execute(exstr, date_time)
    rmamachines = cursor.fetchall()
    for tuple in rmamachines:
        for stuff in tuple:
            stuff = stuff.split('[')[1].split(']')[0].replace("'", '').split(', ')
            allmissing = stuff
            for i in allmissing:
                for x in stillValid:
                    if x == i:
                        ipcount = -1
                        for ip in ont['IP Address']:
                            if x == ip:
                                ipcount += 1
                                x = "<http://root:root@" + x + "/cgi-bin/minerStatus.cgi|" + x + ">"
                                file.write(x + '   *|*   ' + ont.at[ipcount, 'expiryDate'] + '\n')
                            else:
                                ipcount += 1
                        count += 1
    print(str(count) + " machines w/ low ASIC's and valid RMA dates")
    file.close()

    count = 0
    file = open('zerohash.txt', 'w')
    for ip in stillValid:
        exstr = """SELECT zeroHash FROM `Issues_History` WHERE date_time = %s"""
        cursor.execute(exstr, date_time)
    rmamachines = cursor.fetchall()
    for tuple in rmamachines:
        for stuff in tuple:
            stuff = stuff.split('[')[1].split(']')[0].replace("'", '').split(', ')
            allmissing = stuff
            for i in allmissing:
                for x in stillValid:
                    if x == i:
                        ipcount = -1
                        for ip in ont['IP Address']:
                            if x == ip:
                                ipcount += 1
                                url = 'http://' + ip + '/cgi-bin/get_miner_status.cgi'
                                s = requests.Session()
                                uptime = 0
                                try:
                                    r = s.post(url, auth=HTTPDigestAuth('root', 'root'), timeout=3)
                                    data = json.loads(r.text)
                                    uptime = int(data['summary']['elapsed'])
                                    if uptime > 1800:
                                        x = "<http://root:root@" + x + "/cgi-bin/minerStatus.cgi|" + x + ">"
                                        file.write(x + '   *|*   ' + ont.at[ipcount, 'expiryDate'] + '\n')
                                except Exception as e:
                                    print('failed to check uptime of  ' + ip + ' - ' + str(e))
                            else:
                                ipcount += 1
                        count += 1
    print(str(count) + " zero hash machines w/ uptime over 30 minutes\n")
    file.close()
validRMA()
schedule.every().hour.do(validRMA)

while True:
    schedule.run_pending()
    time.sleep(1)
